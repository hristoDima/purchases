INSERT INTO user (id, username, password, first_name, last_name, role)
VALUES (1, 'admin', 'admin', 'Admin', 'Admin', 'ADMIN'),
       (2, 'admin2', 'admin', 'Admin', 'Admin', 'ADMIN');

INSERT INTO transaction_type (type)
VALUES ('Приход'),
       ('Разход');

INSERT INTO payment (payment_method)
VALUES ('В брой'),
       ('С карта');
