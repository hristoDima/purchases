package com.venico.purchases.services.impl;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.models.Transaction;
import com.venico.purchases.repositories.PurchaseRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseServiceImplTest {

    private static final long ID = 1L;
    private static final double MONEY = 1.23;
    private static final String REASON_1 = "reason_1";
    private static final String REASON_2 = "reason_2";

    @Mock
    PurchaseRepository purchaseRepository;

    @InjectMocks
    PurchaseServiceImpl purchaseService;

    @Before
    public void setUp() {
    }

    @Test
    public void getPurchaseByIdOkTest() throws DatabaseProblemException {
        Purchase purchase = createPurchase(MONEY, REASON_1);
        purchase.setId(ID);
        Mockito.when(purchaseRepository.findById(ArgumentMatchers.eq(ID))).thenReturn(Optional.of(purchase));

        Purchase resultPurchase = purchaseService.getPurchaseById(ID);

        assertNotNull(resultPurchase);
        assertEquals(purchase, resultPurchase);
    }

    @Test
    public void getPurchaseByIdNotExistingTest() throws DatabaseProblemException {
        Mockito.when(purchaseRepository.findById(ArgumentMatchers.eq(ID))).thenReturn(Optional.empty());

        Purchase resultPurchase = purchaseService.getPurchaseById(ID);

        assertNull(resultPurchase);
    }

    @Test(expected = DatabaseProblemException.class)
    public void getPurchaseByIdRepositoryExceptionTest() throws DatabaseProblemException {
        Mockito.when(purchaseRepository.findById(ArgumentMatchers.eq(ID))).thenThrow(new DataIntegrityViolationException(""));
        purchaseService.getPurchaseById(ID);
    }

    @Test
    public void getAllPurchasesOkTest() throws DatabaseProblemException {
        List<Purchase> expectedPurchases = List.of(createPurchase(MONEY, REASON_1), createPurchase(MONEY, REASON_2));
        Mockito.when(purchaseRepository.findAll()).thenReturn(expectedPurchases);

        List<Purchase> resultPurchases = purchaseService.getAllPurchases();

        assertNotNull(resultPurchases);
        assertEquals(2, resultPurchases.size());
        assertTrue(expectedPurchases.containsAll(resultPurchases));
    }

    @Test
    public void getAllPurchasesNoPurchasesTest() throws DatabaseProblemException {
        Mockito.when(purchaseRepository.findAll()).thenReturn(Collections.emptyList());

        List<Purchase> resultPurchases = purchaseService.getAllPurchases();

        assertNotNull(resultPurchases);
        assertTrue(resultPurchases.isEmpty());
    }

    @Test(expected = DatabaseProblemException.class)
    public void getAllPurchasesRepositoryExceptionTest() throws DatabaseProblemException {
        Mockito.when(purchaseRepository.findAll()).thenThrow(new DataIntegrityViolationException(""));
        purchaseService.getAllPurchases();
    }

    @Test
    public void getAllPurchasesForUserOkTest() throws DatabaseProblemException {
        List<Purchase> expectedPurchases = List.of(createPurchase(MONEY, REASON_1), createPurchase(MONEY, REASON_2));
        PurchasesUser user = new PurchasesUser();
        Mockito.when(purchaseRepository.getPurchasesByUserOrderByDateDesc(ArgumentMatchers.eq(user))).
                thenReturn(expectedPurchases);

        List<Purchase> resultPurchases = purchaseService.getAllPurchasesForUser(user);

        assertTrue(expectedPurchases.containsAll(resultPurchases));
    }

    @Test
    public void getAllPurchasesForUserNoPurchasesTest() throws DatabaseProblemException {
        PurchasesUser user = new PurchasesUser();
        Mockito.when(purchaseRepository.getPurchasesByUserOrderByDateDesc(ArgumentMatchers.eq(user))).
                thenReturn(Collections.emptyList());

        List<Purchase> resultPurchases = purchaseService.getAllPurchasesForUser(user);

        assertNotNull(resultPurchases);
        assertTrue(resultPurchases.isEmpty());
    }

    @Test(expected = DatabaseProblemException.class)
    public void getAllPurchasesForUserDBProblemTest() throws DatabaseProblemException {
        PurchasesUser user = new PurchasesUser();
        Mockito.when(purchaseRepository.getPurchasesByUserOrderByDateDesc(ArgumentMatchers.eq(user))).
                thenThrow(new DataIntegrityViolationException(""));

        purchaseService.getAllPurchasesForUser(new PurchasesUser());
    }

    @Test
    public void saveNewPurchaseOkTest() throws DatabaseProblemException {
        Purchase purchaseToSave = createPurchase(MONEY, REASON_1);
        Mockito.when(purchaseRepository.save(ArgumentMatchers.eq(purchaseToSave))).
                thenReturn(purchaseToSave);

        Purchase resultPurchases = purchaseService.saveOrUpdatePurchase(purchaseToSave);

        assertEquals(purchaseToSave, resultPurchases);
    }

    @Test(expected = DatabaseProblemException.class)
    public void saveOrUpdatePurchaseDBProblemTest() throws DatabaseProblemException {
        Purchase purchaseToSave = createPurchase(MONEY, REASON_1);
        Mockito.when(purchaseRepository.save(ArgumentMatchers.eq(purchaseToSave))).
                thenThrow(new DataIntegrityViolationException(""));

        purchaseService.saveOrUpdatePurchase(purchaseToSave);
    }

    @Test
    @Ignore
    public void random() {
        Purchase purchase = new Purchase();
        purchase.setMoney(MONEY);
        purchase.setReason(null);
        purchase.setTransactionType(new Transaction("transaction type"));

        Purchase purchase1 = new Purchase();
        purchase1.setMoney(MONEY);
        purchase1.setReason(REASON_1);
        purchase1.setTransactionType(new Transaction("transaction type 2"));

//        PurchaseDTO purchaseDto = new PurchaseDTO();
        BeanUtils.copyProperties(purchase, purchase1);

        Purchase purchase2 = new Purchase();
    }

    private Purchase createPurchase(double money, String reason) {
        Purchase purchase = new Purchase();
        purchase.setMoney(money);
        purchase.setReason(reason);
        return purchase;
    }
}