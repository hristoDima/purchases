package com.venico.purchases.services.impl.IT;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Payment;
import com.venico.purchases.services.PaymentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
// TODO implement tests
public class PaymentServiceImplIntegrationTest {

    @Autowired
    private PaymentService paymentService;

    @Test
    public void getAllPaymentMethodsTest() throws DatabaseProblemException {
        List<Payment> paymentMethods =  paymentService.getAllPaymentMethods();

        assertEquals(2, paymentMethods.size());
    }

//    @Test
//    public void getByIdTest() throws DatabaseProblemException {
//        Payment paymentMethod = paymentService.getById("");
//
//        assertNotNull(paymentMethod);
//    }
}