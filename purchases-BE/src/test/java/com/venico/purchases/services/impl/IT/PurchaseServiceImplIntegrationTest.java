package com.venico.purchases.services.impl.IT;

import com.venico.purchases.dto.PurchaseDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.exceptions.InvalidPurchaseDataException;
import com.venico.purchases.mappers.DataConverter;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.repositories.PurchaseRepository;
import com.venico.purchases.repositories.UserRepository;
import com.venico.purchases.services.PurchaseService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseServiceImplIntegrationTest {

    private static final String PAYMENT_METHOD = "В брой";
    private static final String TRANSACTION_TYPE = "Приход";
    private static final String RANDOM_STRING = "abc";
    private static final double MONEY = 10.10;
    private static final String USERNAME_ADMIN = "admin";
    private static final String USERNAME_ADMIN_2 = "admin2";
    private static final String DATE = "2020-12-13";
    private static final String NEW_PRODUCTS = "new products";

    @Autowired
    UserRepository userRepository;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    DataConverter dataConverter;

    @Autowired
    PurchaseService purchaseService;

    Purchase dbPurchase;

    @Before
    public void setUp() throws ParseException, InvalidPurchaseDataException, DatabaseProblemException {
        dbPurchase = addPurchaseForUserToDB(USERNAME_ADMIN);
    }

    @After
    public void tearDown() {
        purchaseRepository.deleteAll();
    }

    @Test
    public void getAllPurchasesTest() throws DatabaseProblemException {
        List<Purchase> purchases = purchaseService.getAllPurchases();
        assertEquals(1, purchases.size());
    }

    @Test
    public void getPurchasesForUserTest() throws DatabaseProblemException, ParseException, InvalidPurchaseDataException {
        addPurchaseForUserToDB(USERNAME_ADMIN_2);
        PurchasesUser user = userRepository.findByUsername(USERNAME_ADMIN_2);

        List<Purchase> purchases = purchaseService.getAllPurchasesForUser(user);

        assertEquals(1, purchases.size());
    }

    @Test
    public void getPurchaseByIdTest() throws DatabaseProblemException {
        Purchase purchase = purchaseService.getPurchaseById(dbPurchase.getId());

        assertEquals(dbPurchase, purchase);
    }

    @Test
    public void saveNewPurchaseTest() throws
            DatabaseProblemException, ParseException, InvalidPurchaseDataException {
        Purchase purchaseToSave = createPurchaseForUser(USERNAME_ADMIN_2);

        Purchase resultPurchase = purchaseService.saveOrUpdatePurchase(purchaseToSave);
        Purchase expectedResultPurchase = purchaseRepository.findById(purchaseToSave.getId()).orElse(null);

        assertNotNull(expectedResultPurchase);
        assertEquals(expectedResultPurchase, resultPurchase);
    }

    @Test
    public void updateExistingPurchaseTest() throws DatabaseProblemException {
        Purchase purchaseToUpload = dbPurchase;
        purchaseToUpload.setProducts(NEW_PRODUCTS);

        Purchase resultPurchase = purchaseService.saveOrUpdatePurchase(purchaseToUpload);

        Purchase expectedResultPurchase = purchaseRepository.findById(purchaseToUpload.getId()).orElse(null);

        assertEquals(expectedResultPurchase, resultPurchase);
    }

    @Test
    public void deletePurchaseOkTest() throws DatabaseProblemException {
        boolean deleteResult = purchaseService.deletePurchase(dbPurchase);
        assertTrue(deleteResult);

        List<Purchase> purchasesLeft = purchaseService.getAllPurchases();
        assertTrue(purchasesLeft.isEmpty());
    }

    @Test(expected = DatabaseProblemException.class)
    public void deletePurchaseNullIdTest() throws DatabaseProblemException {
        Purchase purchaseToDelete = new Purchase();
        BeanUtils.copyProperties(dbPurchase, purchaseToDelete);
        purchaseToDelete.setId(null);

        boolean deleteResult = purchaseService.deletePurchase(purchaseToDelete);
        assertTrue(deleteResult);

        List<Purchase> purchasesLeft = purchaseService.getAllPurchases();
        assertEquals(1, purchasesLeft.size());
    }

    @Test(expected = DatabaseProblemException.class)
    public void deletePurchaseNotExistingIdTest() throws DatabaseProblemException {
        Purchase purchaseToDelete = new Purchase();
        BeanUtils.copyProperties(dbPurchase, purchaseToDelete);
        purchaseToDelete.setId(1234L);

        boolean deleteResult = purchaseService.deletePurchase(purchaseToDelete);
        assertTrue(deleteResult);

        List<Purchase> purchasesLeft = purchaseService.getAllPurchases();
        assertEquals(1, purchasesLeft.size());
    }

    private Purchase addPurchaseForUserToDB(String username) throws ParseException,
            InvalidPurchaseDataException, DatabaseProblemException {
        Purchase purchase = createPurchaseForUser(username);
        return purchaseRepository.save(purchase);
    }

    private Purchase createPurchaseForUser(String username) throws ParseException,
            InvalidPurchaseDataException, DatabaseProblemException {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date(dateFormatter.parse(DATE).getTime());
        PurchaseDTO purchaseDTO = new PurchaseDTO(null, MONEY, TRANSACTION_TYPE, PAYMENT_METHOD,
                RANDOM_STRING, RANDOM_STRING, convertedDate, null);
        return dataConverter.convertPurchaseDtoToPurchase(purchaseDTO, username);
    }
}
