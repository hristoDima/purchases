package com.venico.purchases.services.impl;

import com.venico.purchases.dto.UserRegistrationDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.models.UserRole;
import com.venico.purchases.repositories.UserRepository;
import com.venico.purchases.services.UserService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Getter
@Service
public class UserServiceImpl implements UserService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public PurchasesUser register(@Valid UserRegistrationDTO userRegistrationDTO) throws DatabaseProblemException {
        PurchasesUser purchasesUser = new PurchasesUser();
        purchasesUser.setUsername(userRegistrationDTO.getUsername());
        purchasesUser.setFirstName(userRegistrationDTO.getFirstName());
        purchasesUser.setLastName(userRegistrationDTO.getLastName());
        purchasesUser.setPassword(getPasswordEncoder().encode(userRegistrationDTO.getPassword()));
        purchasesUser.setRole(UserRole.USER);
        try {
            return getUserRepository().saveAndFlush(purchasesUser);
        }
        catch (Exception e){
            String message = "Error while registering user " + userRegistrationDTO.getUsername() +  " in the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public PurchasesUser findByUsername(String username) throws DatabaseProblemException {
        PurchasesUser user;
        try {
            user = getUserRepository().findByUsername(username);
        }
        catch(Exception e) {
            String message = "Error while searching for user " + username +  " in the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
        return user;
    }

    @Override
    public List<PurchasesUser> findAll() throws DatabaseProblemException {
        List<PurchasesUser> users;
        try {
            users = getUserRepository().findAll();
        }
        catch (Exception e) {
            String message = "Error while searching for users in the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
        return users;
    }

    @Override
    public boolean isUserAdministrator(PurchasesUser user) {
        return user.getRole().equals(UserRole.ADMIN);
    }

}
