package com.venico.purchases.services;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Transaction;

import java.util.List;

public interface TransactionService {
    /**
     * Gets transactions from the DB
     *
     * @return list with found transactions or empty list if none found
     * @throws DatabaseProblemException if problem while getting transaction from DB
     */
    List<Transaction> getAvailableTransactionTypes() throws DatabaseProblemException;

    /**
     * Gets the transaction with given id
     *
     * @param id id of the transaction that will be searched for
     * @return the found {@link Transaction} or null if not found
     * @throws DatabaseProblemException if problem while getting transaction from DB
     */
    Transaction getById(String id) throws DatabaseProblemException;
}
