package com.venico.purchases.services.impl;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.repositories.PurchaseRepository;
import com.venico.purchases.services.PurchaseService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;

@Service
@Getter
public class PurchaseServiceImpl implements PurchaseService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Override
    public Purchase getPurchaseById(long purchaseId) throws DatabaseProblemException {
        try {
            return getPurchaseRepository().findById(purchaseId).orElse(null);
        } catch (Exception e) {
            String message = "Error while getting purchase with id=" + purchaseId + " from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public List<Purchase> getAllPurchasesForUser(PurchasesUser user) throws DatabaseProblemException {
        try {
            return getPurchaseRepository().getPurchasesByUserOrderByDateDesc(user);
        } catch (Exception e) {
            String message = "Error while getting available purchases for user " + user.getUsername() + " from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public List<Purchase> getAllPurchases() throws DatabaseProblemException {
        try {
            return getPurchaseRepository().findAll();
        } catch (Exception e) {
            String message = "Error while getting available purchases from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public Purchase saveOrUpdatePurchase(@Valid Purchase purchase) throws DatabaseProblemException {
        try {
            return getPurchaseRepository().save(purchase);
        } catch (Exception e) {
            String message = "Error in saving purchase to the DB! " + purchase;
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public boolean deletePurchase(Purchase purchase) throws DatabaseProblemException {
        try {
            getPurchaseRepository().deleteById(purchase.getId());
            return true;
        }
        catch (Exception e) {
            String message = "Error in deleting purchase from the DB! " + purchase;
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

}
