package com.venico.purchases.services;

import com.venico.purchases.dto.UserRegistrationDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.PurchasesUser;

import java.util.List;

public interface UserService {

    /**
     * Registers new user in the system
     *
     * @param userRegistrationDTO user to register
     * @return registered user object
     * @throws DatabaseProblemException if problem with saving the new user in the DB
     */
    PurchasesUser register(UserRegistrationDTO userRegistrationDTO) throws DatabaseProblemException;

    /**
     * Finds user by username
     *
     * @param username username of the user
     * @return {@link PurchasesUser} if exists or null if not
     * @throws DatabaseProblemException if problem while getting user from the DB
     */
    PurchasesUser findByUsername(String username) throws DatabaseProblemException;

    /**
     * Finds all users
     * @return List of all users or empty list
     * @throws DatabaseProblemException if problem while getting user from the DB
     */
    List<PurchasesUser> findAll() throws DatabaseProblemException;

    boolean isUserAdministrator(PurchasesUser user);
}
