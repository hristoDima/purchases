package com.venico.purchases.services;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Payment;

import java.util.List;

public interface PaymentService {
    /**
     * Retrieves all available payment methods in the system
     *
     * @return List of {@link Payment} or empty list if none found
     * @throws DatabaseProblemException if problem with getting the data from the DB
     */
    List<Payment> getAllPaymentMethods() throws DatabaseProblemException;

    /**
     * Get payment by given id
     *
     * @param id id of the payment
     * @return {@link Payment} if entity with this id exists, otherwise null
     * @throws DatabaseProblemException if problem with getting the data from the DB
     */
    Payment getById(String id) throws DatabaseProblemException;
}
