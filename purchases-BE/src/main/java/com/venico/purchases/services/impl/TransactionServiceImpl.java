package com.venico.purchases.services.impl;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Transaction;
import com.venico.purchases.repositories.TransactionRepository;
import com.venico.purchases.services.TransactionService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Getter
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public List<Transaction> getAvailableTransactionTypes() throws DatabaseProblemException {
        try {
            return getTransactionRepository().findAll();
        } catch (Exception e) {
            String message = "Error while getting available transaction types from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public Transaction getById(String id) throws DatabaseProblemException {
        try {
            return getTransactionRepository().findById(id).orElse(null);
        } catch (Exception e) {
            String message = "Error while getting  transaction with id " + id + " from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }
}
