package com.venico.purchases.services;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;

import java.util.List;

public interface PurchaseService {
    /**
     * Get purchase by given id
     *
     * @param purchaseId id of the purchase to get
     * @return {@link Purchase} with given id or null of not exists
     * @throws DatabaseProblemException if problem while geting purchase
     */
    Purchase getPurchaseById(long purchaseId) throws DatabaseProblemException;

    /**
     * Get all available purchases for given user
     *
     * @param user user to get purchases for
     * @return list containing aLL purchases
     */
    List<Purchase> getAllPurchasesForUser(PurchasesUser user) throws DatabaseProblemException;

    /**
     * Gets all purchases available in the system
     *
     * @return List with all purchases or empty list if none found
     * @throws DatabaseProblemException if problem with retrieving purchases
     */
    List<Purchase> getAllPurchases() throws DatabaseProblemException;

    /**
     * Adds new purchase in the DB if the id of the object is null or updates existing purchase
     *
     * @param purchase the purchase that will be added or updated
     *                 --@param user     user that the purchase is for
     * @return newly added purchase
     * @throws DatabaseProblemException if problem with getting data from the DB
     */
    Purchase saveOrUpdatePurchase(Purchase purchase) throws
            DatabaseProblemException;

    /**
     * Deletes purchase from the DB
     * @param purchase purchase that will be deleted
     * @return true if operation succeed, false otherwise
     * @throws DatabaseProblemException if problem with deleting data from the DB
     */
    boolean deletePurchase(Purchase purchase) throws DatabaseProblemException;

}
