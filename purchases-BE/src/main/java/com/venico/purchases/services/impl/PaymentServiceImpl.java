package com.venico.purchases.services.impl;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Payment;
import com.venico.purchases.repositories.PaymentRepository;
import com.venico.purchases.services.PaymentService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Getter
public class PaymentServiceImpl implements PaymentService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public List<Payment> getAllPaymentMethods() throws DatabaseProblemException {
        try {
            return getPaymentRepository().findAll();
        } catch (Exception e) {
            String message = "Error while getting available payment methods from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }

    @Override
    public Payment getById(String id) throws DatabaseProblemException {
        try {
            return getPaymentRepository().findById(id).orElse(null);
        } catch (Exception e) {
            String message = "Error while getting  payment method with id " + id + " from the DB";
            log.error(message, e);
            throw new DatabaseProblemException(message, e);
        }
    }
}
