package com.venico.purchases.controllers;

import com.venico.purchases.dto.UserDTO;
import com.venico.purchases.dto.UserRegistrationDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.services.UserService;
import com.venico.purchases.utils.EndpointsUrl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(EndpointsUrl.BASE_URL)
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = EndpointsUrl.REGISTER)
    public void registerUser(@RequestBody @Valid UserRegistrationDTO userRegistrationDTO) throws DatabaseProblemException {
        userService.register(userRegistrationDTO);
    }

    @GetMapping(value = EndpointsUrl.USER)
    public UserDTO getCurrentUser(Principal principal) throws DatabaseProblemException {
        PurchasesUser user = userService.findByUsername(principal.getName());
        UserDTO outputUser = new UserDTO();
        BeanUtils.copyProperties(user, outputUser);

        return outputUser;
    }

}
