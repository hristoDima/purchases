package com.venico.purchases.controllers;

import com.venico.purchases.dto.PurchaseDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.exceptions.InvalidPurchaseDataException;
import com.venico.purchases.mappers.DataConverter;
import com.venico.purchases.mappers.PurchaseMapper;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.services.PurchaseService;
import com.venico.purchases.services.UserService;
import com.venico.purchases.utils.EndpointsUrl;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@RestController
@RequestMapping(path = EndpointsUrl.BASE_URL)
public class PurchaseController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    UserService userService;

    @Autowired
    DataConverter dataConverter;

    @GetMapping(path = EndpointsUrl.ADMIN + EndpointsUrl.PURCHASE)
    public List<PurchaseDTO> getAllPurchases() throws DatabaseProblemException {
        List<Purchase> purchases = getPurchaseService().getAllPurchases();

        return purchases.stream().map(PurchaseMapper::toPurchaseDto).collect(Collectors.toList());
    }

    @GetMapping(path = EndpointsUrl.PURCHASE)
    public List<PurchaseDTO> getAllPurchasesForUser(Principal principal) throws DatabaseProblemException {
        PurchasesUser user = getUserService().findByUsername(principal.getName());
        List<Purchase> purchases = getPurchaseService().getAllPurchasesForUser(user);

        return purchases.stream().map(PurchaseMapper::toPurchaseDto).collect(Collectors.toList());
    }

    @PostMapping(path = EndpointsUrl.PURCHASE)
    public PurchaseDTO addNewPurchase(@RequestBody @Valid PurchaseDTO purchase, Principal principal) throws
            DatabaseProblemException, InvalidPurchaseDataException {
        Purchase purchaseToAdd = getDataConverter().convertPurchaseDtoToPurchase(purchase, principal.getName());

        Purchase purchaseAdded = getPurchaseService().saveOrUpdatePurchase(purchaseToAdd);

        return PurchaseMapper.toPurchaseDto(purchaseAdded);
    }

    @PutMapping(path = EndpointsUrl.PURCHASE + "/{purchaseId}")
    public PurchaseDTO updatePurchase(@PathVariable long purchaseId, @RequestBody @Valid PurchaseDTO purchase,
                                      Principal principal) throws
            DatabaseProblemException, InvalidPurchaseDataException {

        Purchase purchaseToUpdate = getPurchaseService().getPurchaseById(purchaseId);

        if (purchaseToUpdate == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if (purchase.getId() != null && purchase.getId() != purchaseId) {
            String message = "Required purchase is with id=" + purchaseId + ", but the id in the body is id="
                    + purchase.getId();
            log.error(message);
            throw new InvalidPurchaseDataException(message);
        }
        if (purchase.getId() == null) {
            purchase.setId(purchaseId);
        }

        PurchasesUser user = getUserService().findByUsername(principal.getName());
        if (userService.isUserAdministrator(user)) {
            String newPurchaseUser = purchase.getUsername() != null ? purchase.getUsername() : principal.getName();
            Purchase newPurchaseData = getDataConverter().convertPurchaseDtoToPurchase(purchase, newPurchaseUser);
            BeanUtils.copyProperties(newPurchaseData, purchaseToUpdate);
        } else if (principal.getName().equals(purchaseToUpdate.getUser().getUsername())) {
            log.debug("User " + principal.getName() + " updates purchase with id=" + purchaseId
                    + " which belongs to him.");
            if (purchaseToUpdate.getUser().getUsername().equals(purchase.getUsername())) {
                Purchase newPurchaseData = getDataConverter().convertPurchaseDtoToPurchase(purchase, purchase.getUsername());
                BeanUtils.copyProperties(newPurchaseData, purchaseToUpdate);
            } else {
                String message = "User=" + principal.getName() + " has no permission to change " +
                        "the username of the purchase!";
                log.error(message);
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        }
        Purchase updatedPurchase = getPurchaseService().saveOrUpdatePurchase(purchaseToUpdate);
        return PurchaseMapper.toPurchaseDto(updatedPurchase);
    }

    @DeleteMapping(path = EndpointsUrl.PURCHASE + "/{purchaseId}")
    public void deletePurchase(@PathVariable long purchaseId, Principal principal) throws
            DatabaseProblemException, InvalidPurchaseDataException {

        Purchase purchaseToDelete = getPurchaseService().getPurchaseById(purchaseId);

        if (purchaseToDelete == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        PurchasesUser user = getUserService().findByUsername(principal.getName());
        if (!userService.isUserAdministrator(user)) {
            if (user.equals(purchaseToDelete.getUser())) {
                purchaseService.deletePurchase(purchaseToDelete);
            } else {
                String message = "User=" + principal.getName() + " has no permission to delete " +
                        "the purchase!";
                log.error(message);
                throw new ResponseStatusException(HttpStatus.FORBIDDEN);
            }
        } else {
            purchaseService.deletePurchase(purchaseToDelete);
        }
    }
}
