package com.venico.purchases.controllers;

import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.models.Payment;
import com.venico.purchases.models.Transaction;
import com.venico.purchases.services.PaymentService;
import com.venico.purchases.services.TransactionService;
import com.venico.purchases.utils.EndpointsUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = EndpointsUrl.BASE_URL + EndpointsUrl.TRANSACTION)
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public List<Transaction> getTransactions() throws DatabaseProblemException {
        return transactionService.getAvailableTransactionTypes();
    }
    }
