package com.venico.purchases.exceptions;

/**
 * Exception used when repository operations fail
 */
public class DatabaseProblemException extends Exception {

    public DatabaseProblemException(String msg) {
        super(msg);
    }

    public DatabaseProblemException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
