package com.venico.purchases.exceptions;

/**
 * Exception used when the data inside purchase object is not a valid one
 */
public class InvalidPurchaseDataException extends Exception {
    public InvalidPurchaseDataException(String msg) {
        super(msg);
    }

    public InvalidPurchaseDataException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
