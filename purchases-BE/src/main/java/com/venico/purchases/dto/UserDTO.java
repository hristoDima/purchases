package com.venico.purchases.dto;

import com.venico.purchases.models.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private String username;

    private String firstName;

    private String lastName;

    private UserRole role;
}
