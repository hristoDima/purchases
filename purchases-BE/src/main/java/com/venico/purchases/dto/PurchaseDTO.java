package com.venico.purchases.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.sql.Date;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseDTO {

    Long id;

    @NotNull
    private double money;

    @NotNull
    private String transactionType;

    @NotNull
    private String paymentMethod;

    private String reason;

    private String products;

    @NotNull
    private Date date;

    private String username;
}
