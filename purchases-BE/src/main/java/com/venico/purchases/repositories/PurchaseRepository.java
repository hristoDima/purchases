package com.venico.purchases.repositories;

import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

    List<Purchase> getPurchasesByUserOrderByDateDesc(PurchasesUser purchasesUser);
}
