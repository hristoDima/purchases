package com.venico.purchases.repositories;

import com.venico.purchases.models.PurchasesUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<PurchasesUser, Long> {

    PurchasesUser findByUsername(String username);
}
