package com.venico.purchases.utils;

public class EndpointsUrl {
    public static final String BASE_URL = "/api/v1";

    public static final String ADMIN = "/admin";

    public static final String PURCHASE = "/purchase";

    public static final String TRANSACTION = "/transaction";

    public static final String PAYMENT = "/payment";

    public static final String USER = "/user";

    public static final String REGISTER = "/registration";

    public static final String LOGIN = "/login";
}
