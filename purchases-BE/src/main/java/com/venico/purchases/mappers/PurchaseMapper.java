package com.venico.purchases.mappers;

import com.venico.purchases.dto.PurchaseDTO;
import com.venico.purchases.models.Purchase;

public class PurchaseMapper {
    public static Purchase toPurchase(PurchaseDTO purchaseDto) {
        Purchase purchase = new Purchase();
        purchase.setId(purchaseDto.getId());
        purchase.setMoney(purchaseDto.getMoney());
        purchase.setReason(purchaseDto.getReason());
        purchase.setProducts(purchaseDto.getProducts());
        purchase.setDate(purchaseDto.getDate());
        return purchase;
    }

    public static PurchaseDTO toPurchaseDto(Purchase purchase) {
        PurchaseDTO purchaseDto = new PurchaseDTO();
        purchaseDto.setId(purchase.getId());
        purchaseDto.setMoney(purchase.getMoney());
        purchaseDto.setTransactionType(purchase.getTransactionType().getType());
        purchaseDto.setPaymentMethod(purchase.getPaymentMethod().getPaymentMethod());
        purchaseDto.setReason(purchase.getReason());
        purchaseDto.setProducts(purchase.getProducts());
        purchaseDto.setDate(purchase.getDate());
        purchaseDto.setUsername(purchase.getUser().getUsername());
        return purchaseDto;
    }
}
