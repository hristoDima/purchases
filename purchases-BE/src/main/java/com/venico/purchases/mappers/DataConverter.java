package com.venico.purchases.mappers;

import com.venico.purchases.dto.PurchaseDTO;
import com.venico.purchases.exceptions.DatabaseProblemException;
import com.venico.purchases.exceptions.InvalidPurchaseDataException;
import com.venico.purchases.models.Payment;
import com.venico.purchases.models.Purchase;
import com.venico.purchases.models.PurchasesUser;
import com.venico.purchases.models.Transaction;
import com.venico.purchases.services.PaymentService;
import com.venico.purchases.services.PurchaseService;
import com.venico.purchases.services.TransactionService;
import com.venico.purchases.services.UserService;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class DataConverter {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private UserService userService;

    @Autowired
    private PurchaseService purchaseService;

    public Purchase convertPurchaseDtoToPurchase(PurchaseDTO purchaseDto, String username) throws
            InvalidPurchaseDataException, DatabaseProblemException {
        Purchase purchase = PurchaseMapper.toPurchase(purchaseDto);

        Payment payment = getPaymentService().getById(purchaseDto.getPaymentMethod());
        Transaction transaction = getTransactionService().getById(purchaseDto.getTransactionType());
        PurchasesUser user = getUserService().findByUsername(username);

        if (payment != null && transaction != null && user != null) {
            purchase.setPaymentMethod(payment);
            purchase.setTransactionType(transaction);
            purchase.setUser(user);
        } else {
            String message = "Purchase could not be created because of invalid purchaseDto data " + purchaseDto;
            log.error(message);
            throw new InvalidPurchaseDataException(message);
        }
        return purchase;
    }

}
