package com.venico.purchases.security.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JwtUtil implements Serializable {

    public static final String TOKEN_HEADER = "Authorization";
    private static final String TOKEN_TYPE = "JWT";
    private static final String TOKEN_PREFIX = "Bearer ";
    private static final long JWT_TOKEN_VALIDITY = 60 * 30 * 1000;
    private static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    private static final String HEADER_PARAM_TYPE = "type";
    private static final String ROLE = "role";

    public String generateToken(UserDetails userDetails) {
        byte[] signingKey = JWT_SECRET.getBytes();

        List<String> roles = userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return Jwts.builder()
                .setHeaderParam(HEADER_PARAM_TYPE, TOKEN_TYPE)
                .signWith(Keys.hmacShaKeyFor(signingKey), SIGNATURE_ALGORITHM)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .claim(ROLE, roles)
                .compact();
    }

    public Jws<Claims> parseToken(String token) {
        byte[] signingKey = JWT_SECRET.getBytes();
        return Jwts.parser()
                .setSigningKey(signingKey)
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""));
    }

    public boolean checkCorrectSignatureAlgorithm(Jws<Claims> parsedToken) {
        String alg = parsedToken.getHeader().getAlgorithm();
        return alg.equals(SIGNATURE_ALGORITHM.getValue());
    }

    public String getUsernameFromToken(Jws<Claims> parsedToken) {
        return parsedToken.getBody().getSubject();
    }

    public List<SimpleGrantedAuthority> getAuthoritiesFromToken(Jws<Claims> parsedToken) {
        return ((List<?>) parsedToken.getBody()
                .get(ROLE)).stream()
                .map(authority -> new SimpleGrantedAuthority((String) authority))
                .collect(Collectors.toList());
    }

    public void addTokenToResponse(String token, HttpServletResponse response) {
        response.addHeader(TOKEN_HEADER, TOKEN_PREFIX + token);
    }

    public boolean checkTokenHeader(HttpServletRequest request) {
        String token = getToken(request);
        return !ObjectUtils.isEmpty(token) && token.startsWith(TOKEN_PREFIX);
    }

    public String getToken(HttpServletRequest request) {
        return request.getHeader(TOKEN_HEADER);
    }
}