package com.venico.purchases.security;

import com.venico.purchases.security.utils.JwtUtil;
import io.jsonwebtoken.*;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Getter
public class JwtRequestAuthorizationFilter extends OncePerRequestFilter {

    private final Logger log = LogManager.getLogger(this.getClass());

    private final JwtUtil jwtUtil;

    public JwtRequestAuthorizationFilter() {
        this.jwtUtil = new JwtUtil();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);
        if (authentication == null) {
            filterChain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);

    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token;

        if (jwtUtil.checkTokenHeader(request)) {
            token = jwtUtil.getToken(request);
            try {
                Jws<Claims> parsedToken = jwtUtil.parseToken(token);

                if (!jwtUtil.checkCorrectSignatureAlgorithm(parsedToken)) {
                    return null;
                }

                String username = jwtUtil.getUsernameFromToken(parsedToken);
                List<SimpleGrantedAuthority> authorities = jwtUtil.getAuthoritiesFromToken(parsedToken);

                if (!ObjectUtils.isEmpty(username)) {
                    return new UsernamePasswordAuthenticationToken(username, null, authorities);
                }
            } catch (ExpiredJwtException exception) {
                log.warn("Request to parse expired JWT : {} failed : {}", token, exception.getMessage());
            } catch (UnsupportedJwtException exception) {
                log.warn("Request to parse unsupported JWT : {} failed : {}", token, exception.getMessage());
            } catch (MalformedJwtException exception) {
                log.warn("Request to parse invalid JWT : {} failed : {}", token, exception.getMessage());
            } catch (IllegalArgumentException exception) {
                log.warn("Request to parse empty or null JWT : {} failed : {}", token, exception.getMessage());
            }
        }
        return null;
    }


}
