package com.venico.purchases.security;

import com.venico.purchases.security.utils.JwtUtil;
import com.venico.purchases.utils.EndpointsUrl;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String AUTH_LOGIN_URL = EndpointsUrl.BASE_URL + EndpointsUrl.LOGIN;

    private final JwtUtil jwtUtil;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
        setFilterProcessesUrl(AUTH_LOGIN_URL);
        this.jwtUtil = new JwtUtil();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
        JSONObject jsonObject = extractBodyFromRequest(request);

        String username = null;
        String password = null;
        try {
            username = (String) jsonObject.get("username");
            password = (String) jsonObject.get("password");
        } catch (JSONException e) {
            throw new AuthenticationServiceException("No username or password presented in body!", e);
        }

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, password);

        return getAuthenticationManager().authenticate(authenticationToken);
    }

    private JSONObject extractBodyFromRequest(HttpServletRequest request)
            throws AuthenticationServiceException {
        JSONObject jsonObject;
        try {
            String postRequestContent = request.getReader().lines()
                    .collect(Collectors.joining(System.lineSeparator()));
            jsonObject = new JSONObject(postRequestContent);
        } catch (IOException e) {
            throw new AuthenticationServiceException("Can not read request body!", e);
        } catch (JSONException e) {
            throw new AuthenticationServiceException("Body of request not in correct JSON format", e);
        }
        return jsonObject;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain filterChain, Authentication authentication) throws IOException, ServletException {

        User user = ((User) authentication.getPrincipal());

        String token = jwtUtil.generateToken(user);

        jwtUtil.addTokenToResponse(token, response);
    }

}
