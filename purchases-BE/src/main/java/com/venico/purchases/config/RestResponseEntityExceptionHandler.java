package com.venico.purchases.config;

import com.venico.purchases.exceptions.InvalidPurchaseDataException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Custom implementation to handle exception InvalidPurchaseDataException
     *
     * @param ex      the target InvalidPurchaseDataException exception
     * @param request the current request
     * @return ResponseEntity with error information
     */
    @ExceptionHandler(value = InvalidPurchaseDataException.class)
    protected ResponseEntity<Object> handleInvalidDataException(InvalidPurchaseDataException ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();

        String bodyOfResponse = "";

        return handleExceptionInternal(ex, bodyOfResponse, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
    }

    //    @ExceptionHandler(value = {InvalidPurchaseDataException.class, IllegalStateException.class})
    //    protected ResponseEntity<Object> handleConflict(
    //            RuntimeException ex, WebRequest request) {
    //        String bodyOfResponse = "This should be application specific";
    //        return handleExceptionInternal(ex, bodyOfResponse,
    //                new HttpHeaders(), HttpStatus.CONFLICT, request);
    //    }
}
