package com.venico.purchases.models;

public enum UserRole {
    USER,
    ADMIN
}
