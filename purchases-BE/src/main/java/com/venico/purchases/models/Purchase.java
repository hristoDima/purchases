package com.venico.purchases.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Entity
@Table(name = "money")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private double money;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transaction_type", referencedColumnName = "type", nullable = false)
    private Transaction transactionType;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payment_method", referencedColumnName = "payment_method", nullable = false)
    private Payment paymentMethod;

    private String reason;

    private String products;

    @NotNull
    private Date date;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
    private PurchasesUser user;
}
