ALTER TABLE money
    ADD COLUMN user_id INT(10) NOT NULL DEFAULT 1;

ALTER TABLE money
    ADD CONSTRAINT FK_money_user_map FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE ON DELETE CASCADE;
