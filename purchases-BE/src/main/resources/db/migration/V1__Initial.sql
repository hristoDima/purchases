CREATE TABLE `payment`
(
    `payment_method` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL PRIMARY KEY
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `transaction_type`
(
    `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL PRIMARY KEY
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `money`
(
    `id` int(10) unsigned AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `money` double unsigned NOT NULL DEFAULT 0,
    `transaction_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL REFERENCES `transaction_type` (`type`) ON UPDATE CASCADE,
    `payment_method` varchar(50) COLLATE utf8_unicode_ci NOT NULL REFERENCES `payment` (`payment_method`) ON UPDATE CASCADE,
    `reason` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    `products` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
    `date` date NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

CREATE TABLE `user`
(
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `password` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `firs_name` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `last_name` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL,
    `role` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci  NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;