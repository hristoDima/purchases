import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "src/app/services/authentication.service";
import {Dialog} from "primeng/dialog";
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {ResizableDialogUtil} from "src/app/utils/ResizableDialogUtil";
import {Observable} from "rxjs";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @Input() isVisible = false;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  @Output() userAuthenticated = new EventEmitter<boolean>();
  @ViewChild('dialog') dialog: Dialog | undefined;
  loginForm: any;

  loginErrorMessage: string = "Sorry, username or password does not match";

  constructor(private fb: FormBuilder,
              private authenticationService: AuthenticationService,
              private breakpointsObserver: BreakpointObserver
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });

    this.breakpointsObserver.observe(Breakpoints.XSmall).subscribe(
      result => {
        if (this.dialog) {
          result.matches ? ResizableDialogUtil.maximizeDialog(this.dialog)
            : ResizableDialogUtil.minimizeDialog(this.dialog);
        }
      }
    )
  }

  /**
   * Executes when form is submitted and tries to login user
   * @param form form with login data that will be submitted
   */
  onSubmit(form: FormGroup): void {
    this.authenticationService.authenticate(form.value).subscribe(
      isAuthenticated => {
        if (isAuthenticated instanceof Observable) {
          isAuthenticated.subscribe(
            result => {
              this.userAuthenticated.emit(result);
            });
        } else {
          this.userAuthenticated.emit(isAuthenticated);
        }
        form.reset();
        this.isVisible = false;
      }
    );
  }

  /**
   * Executes when login modal hides
   */
  onDialogHide(): void {
    this.isVisibleChange.emit(this.isVisible);
  }

  /**
   * Executes when login modal is shown and maximizes it if on small screens
   */
  onDialogShow(): void {
    this.maximizeDialogIfNeeded();
  }

  /**
   * Checks the screen size and maximizes the login dialog in needed
   */
  private maximizeDialogIfNeeded(): void {
    if (this.breakpointsObserver.isMatched(Breakpoints.XSmall)) {
      if (this.dialog) {
        ResizableDialogUtil.maximizeDialog(this.dialog);
      }
    }
  }

}
