import { Component, OnInit } from '@angular/core';
import {TransactionType} from "src/app/models/TransactionType";
import {PaymentMethod} from "src/app/models/PaymentMethod";
import {DataService} from "src/app/services/data.service";

@Component({
  selector: 'app-display-purchases',
  templateUrl: './display-purchases.component.html',
  styleUrls: ['./display-purchases.component.scss']
})
export class DisplayPurchasesComponent implements OnInit {

  transactionTypes: TransactionType[] = [];
  paymentMethods: PaymentMethod[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.getTransactionTypes().subscribe(res => this.transactionTypes = res);
    this.dataService.getPaymentMethods().subscribe(res => this.paymentMethods = res);
  }

}
