import {Component, OnInit} from '@angular/core';
import {DataService} from 'src/app/services/data.service';
import {Purchase} from 'src/app/models/Purchase';
import {ConfirmationService, SelectItem} from "primeng/api";
import {Router} from "@angular/router";
import {AuthenticationService} from "src/app/services/authentication.service";
import {User} from "src/app/models/User";
import {Role} from "src/app/models/Role";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit {

  purchases: Purchase[] = [];
  columns: any[] = [];
  user: User | null | undefined;

  private transactionTypes: Array<SelectItem> = [];
  private paymentMethods: Array<SelectItem> = [];

  private purchaseToEdit: Purchase | undefined;

  constructor(private dataService: DataService,
              private router: Router,
              private authenticationService: AuthenticationService,
              private confirmationService: ConfirmationService) {
    this.authenticationService.user.subscribe(
      user => {
        this.user = user;
      }
    );
  }

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    }
    this.loadTableData();
  }

  /**
   * Gets the needed data that is loaded inside the table
   */
  private loadTableData() {
    this.dataService.getPurchases().subscribe(result => {
        this.purchases = result;
        this.purchases.forEach(purchase => purchase.date = new Date(purchase.date))
      }
    );
    this.dataService.getTransactionTypes().subscribe(transactionTypes =>
      transactionTypes.forEach(transactionType => this.transactionTypes.push(
        {label: transactionType.type, value: transactionType.type})
      )
    );
    this.dataService.getPaymentMethods().subscribe(paymentMethods =>
      paymentMethods.forEach(paymentMethod => this.paymentMethods.push(
        {label: paymentMethod.paymentMethod, value: paymentMethod.paymentMethod})
      )
    );
    this.populateColumns();
  }

  /**
   * Populates the columns object with the fields and their properties and
   */
  private populateColumns() {
    this.columns = [
      {field: 'money', header: "ui_money", sortable: true, type: "text", filterType: "numeric", editable: true},
      {
        field: 'transactionType',
        subfield: "type",
        header: "ui_transaction_type",
        sortable: true,
        type: "dropdown",
        filterType: "dropdown",
        placeholder: "ui_transaction_type",
        filterOptions: this.transactionTypes,
        editable: true
      },
      {
        field: 'paymentMethod',
        subfield: "paymentMethod",
        header: "ui_payment_method",
        sortable: true,
        type: "dropdown",
        filterType: "dropdown",
        placeholder: "ui_payment_method",
        filterOptions: this.paymentMethods,
        editable: true
      },
      {field: 'reason', header: "ui_reason", sortable: true, type: "text", filterType: "text", editable: true},
      {field: 'products', header: "ui_products", sortable: true, type: "textarea", filterType: "text", editable: true},
      {field: 'date', header: "ui_date", sortable: true, type: "date", filterType: "date", editable: true},
    ]
  }

  /**
   * Initializes the row edit operation
   * @param purchase purchase that will be edited
   */
  onRowEditInit(purchase: Purchase) {
    this.purchaseToEdit = Object.assign({}, purchase);
  }

  /**
   * Cancels the row edit operation
   * @param purchase purchase that is being edited
   * @param rowIndex index of the row that is being edited
   */
  onRowEditCancel(purchase: Purchase, rowIndex: number) {
    this.purchases[rowIndex] = this.purchaseToEdit ? this.purchaseToEdit : this.purchases[rowIndex];
    delete this.purchaseToEdit;
  }

  /**
   * Saves the edited purchase after save of row
   * @param purchase purchase to be saved
   */
  onRowEditSave(purchase: Purchase) {
    this.dataService.updatePurchase(purchase).subscribe();
  }

  /**
   * Displays confirmation popup and if confirmed deletes the purchase
   * @param purchase purchase to be deleted
   */
  deletePurchase(purchase: Purchase) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete purchase for <br>' + purchase.reason +
        " <br> with bought <br>" + purchase.products,
      header: 'Delete confirmation',
      // icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.dataService.deletePurchase(purchase).subscribe(
          () => this.purchases = this.purchases.filter(purch => purch.id !== purchase.id)
        );
      }
    })
  }

  /**
   * Checks if the user is admin
   */
  get isAdmin(): boolean | null | undefined {
    return this.user && this.user.role === Role.USER;
  }

}

