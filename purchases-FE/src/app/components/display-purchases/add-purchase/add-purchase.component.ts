import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataService} from "src/app/services/data.service";
import {TransactionType} from "src/app/models/TransactionType";
import {PaymentMethod} from "src/app/models/PaymentMethod";

@Component({
  selector: 'app-add-purchase',
  templateUrl: './add-purchase.component.html',
  styleUrls: ['./add-purchase.component.scss']
})
export class AddPurchaseComponent implements OnInit {

  addPurchaseForm: any;
  @Input() transactionTypes: TransactionType[] = [];
  @Input() paymentMethods: PaymentMethod[] = [];

  constructor(private fb: FormBuilder,
              private dataService: DataService) {
  }

  ngOnInit() {
    this.addPurchaseForm = this.fb.group({
      money: [null, Validators.required],
      transactionType: [null, Validators.required],
      paymentMethod: [null, Validators.required],
      reason: [],
      products: [],
      date: [null, Validators.required]
    })
  }

  /**
   * Adds the purchase in the system.
   * Executed when add purchase form is submitted.
   * @param form the FormGroup containing new purchase info
   */
  onSubmit(form: FormGroup) {
    let transactionControl = form.controls.transactionType;
    if (transactionControl.value.type) {
      transactionControl.setValue(transactionControl.value.type);
    }

    let paymentControl = form.controls.paymentMethod;
    if (paymentControl.value.paymentMethod) {
      paymentControl.setValue(paymentControl.value.paymentMethod);
    }

    this.dataService.addNewPurchase(form.value).subscribe(
      () => {
        form.reset();
      }
    );
  }

}
