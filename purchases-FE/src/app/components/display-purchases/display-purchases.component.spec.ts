import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPurchasesComponent } from './display-purchases.component';

describe('DisplayPurchasesComponent', () => {
  let component: DisplayPurchasesComponent;
  let fixture: ComponentFixture<DisplayPurchasesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisplayPurchasesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayPurchasesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
