import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {Dialog} from "primeng/dialog";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {ResizableDialogUtil} from "../../utils/ResizableDialogUtil";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  @Input() isVisible = false;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  @ViewChild('dialog') dialog: Dialog | undefined;

  registrationForm: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService,
              private breakpointsObserver: BreakpointObserver) {
  }

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
      repeatPassword: [null, Validators.required],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required]
    }, {validators: this.checkPasswords});

    this.breakpointsObserver.observe(Breakpoints.XSmall).subscribe(
      result => {
        if (this.dialog) {
          result.matches ? ResizableDialogUtil.maximizeDialog(this.dialog)
            : ResizableDialogUtil.minimizeDialog(this.dialog);
        }
      }
    )
  }

  /**
   * Validator function that checks if the values in password and repeat password are same
   * @param group the form group which contains values that will be checked
   */
  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.value.password;
    let confirmPass = group.value.repeatPassword;
    return pass === confirmPass ? null : {notSame: true}
  }

  /**
   * Executes when form is submitted
   * @param form form that is submitted
   */
  onSubmit(form: FormGroup) {
    this.authenticationService.register(form.value).subscribe(
      () => {
        this.isVisible = false;
        form.reset();
      }
    );
  }

  /**
   * Executes when login modal hides
   */
  onDialogHide(): void {
    this.isVisibleChange.emit(this.isVisible);
  }

  /**
   * Executes when login modal is shown and maximizes dialog if needed
   */
  onDialogShow(): void {
    this.maximizeDialogIfNeeded();
  }

  /**
   * Checks device screen and maximizes the registration dialog if needed
   */
  private maximizeDialogIfNeeded(): void {
    if (this.breakpointsObserver.isMatched(Breakpoints.XSmall)) {
      if (this.dialog) {
        ResizableDialogUtil.maximizeDialog(this.dialog);
      }
    }
  }

}
