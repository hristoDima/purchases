import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "src/app/services/data.service";
import {SelectItem} from "primeng/api";

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  @Input() reason: string | undefined = "";
  @Output() reasonChange = new EventEmitter<string>();

  @Input() transactionType: string | undefined = "";
  @Output() transactionTypeChange = new EventEmitter<string>();

  @Input() paymentMethod: string | undefined = "";
  @Output() paymentMethodChange = new EventEmitter<string>();

  @Output() onFilterChange = new EventEmitter<void>()

  transactionTypes: SelectItem[] = [];
  paymentMethods: SelectItem[] = [];

  selectedTransactionType: any;
  selectedPaymentMethod: any;

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getTransactionTypes().subscribe(
      transactionTypes => {
        transactionTypes.forEach(transactionType => {
          this.transactionTypes.push({label: transactionType.type, value: transactionType.type});

          if (this.transactionType === transactionType.type)
            this.selectedTransactionType = this.transactionType;
        });
      }
    );

    this.dataService.getPaymentMethods().subscribe(
      paymentMethods => {
        paymentMethods.forEach(paymentMethod => {
          this.paymentMethods.push({label: paymentMethod.paymentMethod, value: paymentMethod.paymentMethod});

          if (this.paymentMethod === paymentMethod.paymentMethod)
            this.selectedPaymentMethod = paymentMethod.paymentMethod;
        })
      }
    );
  }

}
