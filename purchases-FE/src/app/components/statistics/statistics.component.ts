import {Component, OnInit} from '@angular/core';
import {Purchase} from "src/app/models/Purchase";
import {SelectItem} from "primeng/api";
import {StatisticsService} from "src/app/services/statistics.service";
import {ActivatedRoute} from "@angular/router";
import {Constants} from "src/app/utils/Constants";

export enum StatisticType {
  daily,
  monthly,
  yearly
}

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  public type: StatisticType | undefined;

  public header: string | undefined;

  public rangeStartDate: Date | undefined;
  public rangeEndDate: Date | undefined;
  public rangeDates: any | undefined;

  public rangeReasons: string[] = [""];
  public rangePaymentMethods: string[] = [""];
  public rangeTransactionTypes: string[] = [""];

  private randomColor: string[] = [this.getRandomColor()];

  public statisticData: any;

  //selected years comparison

  // public comparisonYears: Date[] = [];
  // public yearComparisonData: any;
  //
  // public comparisonReasons: string[] = [];
  // public comparisonPaymentMethods: string[] = [];
  // public comparisonTransactionTypes: string[] = [];
  //
  // public comparisonReason: string = "";
  // public comparisonPaymentMethod: string = "";
  // public comparisonTransactionType: string = "";

  constructor(private statisticsService: StatisticsService,
              private route: ActivatedRoute) {
  }

  /**
   * Method used to track the rendered component when ngFor is used
   * @param index
   * @param item
   */
  trackByFn(index: any, item: any) {
    return index;
  }

  ngOnInit() {
    let type = this.route.snapshot.paramMap.get(Constants.STATISTIC_TYPE);
    if (type === Constants.STATISTIC_TYPE_YEAR) {
      this.type = StatisticType.yearly;
      this.header = "ui_statistics_header_range_of_years";
    } else if (type === Constants.STATISTIC_TYPE_MONTH) {
      this.type = StatisticType.monthly;
      this.header = "ui_statistics_header_range_of_months";
    } else if (type === Constants.STATISTIC_TYPE_DAYS) {
      this.type = StatisticType.daily;
      this.header = "ui_statistics_header_range_of_days";
    }
  }

  /**
   * Generates random color
   * @returns string of type #123456
   */
  private getRandomColor(): string {
    return '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
  }

  /**
   * Adds new filter with empty values that will be displayed in the chart as new column
   */
  addNewDatesRangeFilter() {
    this.rangeReasons.push("");
    this.rangePaymentMethods.push("");
    this.rangeTransactionTypes.push("");
    this.randomColor.push(this.getRandomColor());
  }

  /**
   * Renders the dates range chart
   */
  rerenderDatesRangeChart() {
    if (this.type == StatisticType.daily) {
      this.rangeStartDate = this.rangeDates[0];
      this.rangeEndDate = this.rangeDates[1];
    }

    if (this.rangeStartDate && this.rangeEndDate) {
      let rangesSelected: Date[] = this.getDatesInSelectedRange(this.rangeStartDate, this.rangeEndDate);
      let datasets = [];

      for (let i = 0; i < this.rangeReasons.length; i++) {
        datasets.push(
          {
            label: 'Filter ' + i,
            backgroundColor: this.randomColor[i],
            data: new Array<number>()
          }
        );
      }

      let purchasesInPeriod = this.getPurchasesInPeriod();
      for (let i = 0; i < datasets.length; i++) {
        let filteredPurchases = this.statisticsService.filterPurchases(purchasesInPeriod,
          this.rangePaymentMethods[i], this.rangeTransactionTypes[i], this.rangeReasons[i]);

        datasets[i].data = this.getMoneySpentEachDate(rangesSelected, filteredPurchases);
      }

      this.statisticData = {
        labels: this.convertDatesToLabels(rangesSelected),
        datasets: datasets
      };
    }
  }

  /**
   * Gets the dates in the selected range based on statistic type:
   * If the type is yearly - gets all years between start and endDate,
   * if the type is month - gets all months between start and endDate,
   * etc.
   * @param startDate the start date to get dates between
   * @param endDate the end date to get dates between
   * @returns array with dates
   */
  private getDatesInSelectedRange(startDate: Date, endDate: Date): Date[] {
    let rangesSelected = [];
    let currentDate = new Date(startDate.getTime());

    while (currentDate <= endDate) {
      rangesSelected.push(new Date(currentDate.getTime()));
      if (this.type == StatisticType.yearly) {
        currentDate.setFullYear(currentDate.getFullYear() + 1);
      } else if (this.type == StatisticType.monthly) {
        currentDate.setMonth(currentDate.getMonth() + 1);
      } else if (this.type == StatisticType.daily) {
        currentDate.setDate(currentDate.getDate() + 1);
      }
    }
    return rangesSelected;
  }

  /**
   * Based on the statistic type gets the purchases for the selected days range
   * @returns array with purchases that match the selected dates range
   */
  private getPurchasesInPeriod(): Purchase[] {
    if (this.type == StatisticType.yearly) {
      return this.statisticsService.getPurchasesInPeriod(this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null, null, null, null, null,
        null, null, null);
    } else if (this.type == StatisticType.monthly) {
      return this.statisticsService.getPurchasesInPeriod(this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null, this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null,
        null, null, null, null, null);
    } else if (this.type == StatisticType.daily) {
      return this.statisticsService.getPurchasesInPeriod(this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null, this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null, this.rangeStartDate ? this.rangeStartDate : null,
        this.rangeEndDate ? this.rangeEndDate : null, null, null, null);
    }
    return [];
  }

  /**
   * Calculates sum of money spend for each date and puts it in array
   * @param dates the dates to get money for
   * @param purchases purchases for which the money will be summed
   * @returns number array containing sum of money for each date
   */
  private getMoneySpentEachDate(dates: Date[], purchases: Purchase[]): number[] {
    let moneySpentEachDate = [];
    for (let date of dates) {
      let moneySpent = this.calculateMoneySpentEachDate(date, purchases);
      moneySpentEachDate.push(moneySpent);
    }
    return moneySpentEachDate;
  }

  /**
   * Depending on the statistic type, filters the purchases to match same date and
   * sums the money for each purchase
   * @param date the date to get the sum of money for
   * @param purchases purchases to get the money for
   * @returns summed money for purchases after filtered by date
   */
  private calculateMoneySpentEachDate(date: Date, purchases: Purchase[]): number {
    let moneySpent = 0;

    if (this.type == StatisticType.yearly) {
      purchases
        .filter(purchase => new Date(purchase.date).getFullYear() === date.getFullYear())
        .forEach(purchase => moneySpent += purchase.money);
    } else if (this.type == StatisticType.monthly) {
      purchases
        .filter(purchase => {
          let purchaseDate = new Date(purchase.date);
          return purchaseDate.getFullYear() === date.getFullYear() &&
            purchaseDate.getMonth() === date.getMonth();
        })
        .forEach(purchase => moneySpent += purchase.money);
    } else if (this.type == StatisticType.daily) {
      purchases
        .filter(purchase => {
          let purchaseDate = new Date(purchase.date);
          return purchaseDate.getFullYear() === date.getFullYear() &&
            purchaseDate.getMonth() === date.getMonth() &&
            purchaseDate.getDate() === date.getDate();
        })
        .forEach(purchase => moneySpent += purchase.money);
    }
    return moneySpent;
  }

  /**
   * Converts the given dates to labels that will be displayed based on statistic type
   * @param dates dates array to be converted
   * @returns array with labels for each date
   */
  private convertDatesToLabels(dates: Date[]): Array<any> {
    let labels: Array<any> = [];

    let allMonths: SelectItem[] = this.statisticsService.getMonths();
    let daysFormatOptions: Intl.DateTimeFormatOptions = {day: 'numeric', month: 'numeric', year: '2-digit'};

    for (let date of dates) {
      if (this.type == StatisticType.yearly) {
        labels.push(date.getFullYear());
      } else if (this.type == StatisticType.monthly) {
        labels.push(allMonths[date.getMonth()].label + " " + date.getFullYear())
      } else if (this.type == StatisticType.daily) {
        labels.push(date.toLocaleString("bg-BG", daysFormatOptions));
      }
    }

    return labels;
  }

  // addComparisonCalendar() {
  //   this.comparisonYears.push(new Date());
  //   this.comparisonReasons.push("");
  //   this.comparisonPaymentMethods.push("");
  //   this.comparisonTransactionTypes.push("");
  // }
  //
  // rerenderYearComparisonChart() {
  //   let yearsSelected = [];
  //   let moneySpentEachYear = [];
  //
  //   for (let i = 0; i < this.comparisonYears.length; i++) {
  //     yearsSelected.push(this.comparisonYears[i].getFullYear());
  //
  //     let moneySpent = 0;
  //     this.statisticsService.getPurchasesInPeriod(this.comparisonYears[i], this.comparisonYears[i],
  //       null, null, null, null, this.comparisonPaymentMethods[i],
  //       this.comparisonTransactionTypes[i], this.comparisonReasons[i]).forEach(purchase => moneySpent += purchase.money);
  //
  //     moneySpentEachYear.push(moneySpent);
  //   }
  //
  //   this.yearComparisonData = {
  //     labels: yearsSelected,
  //     datasets: [
  //       {
  //         label: 'Money spent',
  //         backgroundColor: '#1e8909',
  //         data: moneySpentEachYear
  //       }
  //     ]
  //   };
  // }

}
