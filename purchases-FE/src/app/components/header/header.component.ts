import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from "src/app/services/authentication.service";
import {Router} from "@angular/router";
import {User} from "src/app/models/User";
import {TranslationService} from "src/app/services/translation.service";
import {CustomMenuItem} from "src/app/models/CustomMenuItem";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() isAdmin: boolean | null | undefined = false;
  user: User | null | undefined;

  items: CustomMenuItem[] = [];

  languageOptions: string[] | undefined;
  selectedLanguage: string | undefined;

  displayLogin = false;
  displayRegistration = false;

  constructor(private authenticationService: AuthenticationService,
              private translationService: TranslationService,
              private router: Router) {
    this.authenticationService.user.subscribe(
      user => {
        this.user = user;

        if (user) {
          this.items = this.getUserLoggedInMenus();
          this.translateMenus();
        } else {
          this.items = this.getUserNotLoggedInMenus();
          this.translateMenus();
        }
      }
    );
  }

  ngOnInit() {
    this.languageOptions = this.translationService.getAvailableLanguages();
    this.selectedLanguage = this.translationService.getCurrentLanguage();

    this.translationService.onLanguageChange().subscribe(() => this.translateMenus());
  }

  /**
   * Iterates over the menus and translates them
   */
  private translateMenus() {
    this.items.forEach(menuItem => {
      if (menuItem.labelTranslationKey)
        menuItem.label = this.translationService.getTranslation(menuItem.labelTranslationKey);

      if (menuItem.items) {
        menuItem.items.forEach(menuItem => {
          if (menuItem.labelTranslationKey)
            menuItem.label = this.translationService.getTranslation(menuItem.labelTranslationKey);
        });
      }
    });
  }

  /**
   * Logouts the user
   */
  private logout() {
    this.authenticationService.logout();
  }

  /**
   * If the user is logged in redirects the view to /purchases
   * @param isUserLoggedIn boolean indicating if the user user is logged in or not
   */
  public redirectUserOnLogin(isUserLoggedIn: boolean) {
    if (isUserLoggedIn) {
      this.router.navigate(['/purchases']);
    }
  }

  /**
   * Gets the menus that should be displayed when user is not logged in
   * @returns Array with menus that should be displayed
   */
  private getUserNotLoggedInMenus(): CustomMenuItem[] {
    return [
      {
        labelTranslationKey: 'ui_menu_login',
        // icon: 'pi pi-fw pi-file',
        command: () => {
          this.displayLogin = true;
        }
      },
      {
        labelTranslationKey: 'ui_menu_registration',
        command: () => {
          this.displayRegistration = true;
        }
      }
    ]
  }

  /**
   * Gets the menus that should be displayed when user is logged in
   * @returns Array with menus that should be displayed
   */
  private getUserLoggedInMenus(): CustomMenuItem[] {
    return [
      {
        labelTranslationKey: 'ui_menu_show_purchases',
        routerLink: ['/purchases']
      },
      {
        labelTranslationKey: 'ui_menu_show_statistics',
        items: [
          {labelTranslationKey: 'ui_menu_yearly_statistics', routerLink: ['/statistics/yearly']},
          {labelTranslationKey: 'ui_menu_monthly_statistics', routerLink: ['/statistics/monthly']},
          {labelTranslationKey: 'ui_menu_daily_statistics', routerLink: ['/statistics/days-range']}
        ]
      },
      {
        labelTranslationKey: 'ui_menu_logout',
        command: () => this.logout()
      }
    ]
  }

  /**
   * Changes language in the system with the given one
   * @param selectedLanguage new language that will be used for messages
   */
  changeLanguage(selectedLanguage: string): void {
    this.translationService.changeLanguage(selectedLanguage);
  }

}
