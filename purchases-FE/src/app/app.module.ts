import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from 'src/app/app-routing.module';
import {AppComponent} from 'src/app/app.component';
import {TableComponent} from 'src/app/components/display-purchases/table/table.component';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AddPurchaseComponent} from 'src/app/components/display-purchases/add-purchase/add-purchase.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DropdownModule} from 'primeng/dropdown';
import {InputNumberModule} from 'primeng/inputnumber';
import {InputTextModule} from 'primeng/inputtext';
import {SelectButtonModule} from 'primeng/selectbutton';
import {RippleModule} from "primeng/ripple";
import {CalendarModule} from 'primeng/calendar';
import {LoginComponent} from 'src/app/components/login/login.component';
import {HeaderComponent} from 'src/app/components/header/header.component';
import {RegisterComponent} from 'src/app/components/register/register.component';
import {MenubarModule} from "primeng/menubar";
import {DialogModule} from 'primeng/dialog';
import {PasswordModule} from 'primeng/password';
import {DisplayPurchasesComponent} from 'src/app/components/display-purchases/display-purchases.component';
import {HomePageComponent} from 'src/app/components/home-page/home-page.component';
import {TokenInterceptor} from "src/app/interceptors/token-interceptor";
import {AccordionModule} from 'primeng/accordion';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {ErrorInterceptor} from "src/app/interceptors/error-interceptor";
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {ConfirmationService, MessageService} from "primeng/api";
import {ToastModule} from 'primeng/toast';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ChartModule} from 'primeng/chart';
import {StatisticsComponent} from 'src/app/components/statistics/statistics.component';
import {FiltersComponent} from 'src/app/components/statistics/filters/filters.component';
import {TabViewModule} from 'primeng/tabview';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {TranslationService} from "src/app/services/translation.service";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    AddPurchaseComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    DisplayPurchasesComponent,
    HomePageComponent,
    StatisticsComponent,
    FiltersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    ButtonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    DropdownModule,
    InputNumberModule,
    InputTextModule,
    SelectButtonModule,
    RippleModule,
    CalendarModule,
    MenubarModule,
    DialogModule,
    PasswordModule,
    AccordionModule,
    InputTextareaModule,
    MessagesModule,
    MessageModule,
    ToastModule,
    FormsModule,
    ConfirmDialogModule,
    ChartModule,
    TabViewModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: MessageService},
    {provide: ConfirmationService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
