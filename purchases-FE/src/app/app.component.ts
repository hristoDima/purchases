import {Component, OnInit} from '@angular/core';
import {PrimeNGConfig} from 'primeng/api';
import {User} from "src/app/models/User";
import {AuthenticationService} from "src/app/services/authentication.service";
import {Role} from "src/app/models/Role";
import {TranslationService} from "src/app/services/translation.service";
import { Calendar } from 'primeng/calendar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  user: User | null | undefined;

  constructor(private primengConfig: PrimeNGConfig,
              private authenticationService: AuthenticationService,
              private translationService: TranslationService
  ) {

    this.authenticationService.user.subscribe(userObservable => this.user = userObservable);
  }

  ngOnInit() {
    this.primengConfig.ripple = true;

    this.translationService.onLanguageChange().subscribe(translation => {
      this.primengConfig.setTranslation(translation.translations["primeng"]);
    })

    Calendar.prototype.getDateFormat = () => 'yy-mm-dd';
  }

  get isAdmin(): boolean | null | undefined {
    return this.user && this.user.role === Role.ADMIN
  }
}
