import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StorageService} from "src/app/services/storage.service";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private readonly TOKEN_HEADER = "Authorization";

  constructor(private storageService: StorageService) {
  }

  intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let tokenValue = this.storageService.getToken()
    if (tokenValue == null) {
      return next.handle(httpRequest);
    }

    const modifiedReq = httpRequest.clone({
      headers: httpRequest.headers.set(this.TOKEN_HEADER, tokenValue),
    });
    return next.handle(modifiedReq);
  }
}
