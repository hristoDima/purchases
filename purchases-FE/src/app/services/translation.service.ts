import {EventEmitter, Injectable} from '@angular/core';
import {LangChangeEvent, TranslateService} from "@ngx-translate/core";
import {StorageService} from "./storage.service";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  private readonly LOCALE = "language";

  constructor(private translate: TranslateService,
              private storageService: StorageService) {
    translate.addLangs(this.getAvailableLanguages());
    translate.setDefaultLang('en');

    let language = this.getLanguageFromStorage();
    this.changeLanguage(language);
  }

  /**
   * Returns languages available in the system
   * @returns Array of strings with available languages
   */
  getAvailableLanguages(): string[] {
    return ['en', 'bg'];
  }

  /**
   * Checks if there is selected language in the storage and if presented returns it,
   * else checks if the browser language is inside the available in the system languages and
   * returns it and if not - returns 'en'
   * @returns string containing language
   */
  getLanguageFromStorage(): string {
    let lang = this.storageService.getItemInLocal(this.LOCALE);
    if (!lang) {
      const browserLang = this.translate.getBrowserLang();
      lang = browserLang?.match(/en|bg/) ? browserLang : 'en';
    }
    return lang;
  }

  /**
   * Gets current used language for translations
   * @returns string containing language
   */
  getCurrentLanguage(): string {
    return this.translate.currentLang;
  }

  /**
   * Changes the language in the application
   * @param language new language to use for translations
   */
  changeLanguage(language: string): void {
    this.storageService.setItemInLocal(this.LOCALE, language);
    this.translate.use(language);
  }

  /**
   * Event emitter that notifies the subscribers when the language is changed
   * @returns An EventEmitter to listen to lang change events onLanguageChange().subscribe(
   * (params: LangChangeEvent) => { // do something })
   */
  onLanguageChange(): EventEmitter<LangChangeEvent> {
    return this.translate.onLangChange;
  }

  /**
   * Gets the translation of the key in the current used language
   * @param key key to translate
   */
  getTranslation(key: string): any {
    return this.translate.instant(key);
  }

}
