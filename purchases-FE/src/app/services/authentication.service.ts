import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of} from "rxjs";
import {User} from "src/app/models/User";
import {StorageService} from "./storage.service";
import {catchError, map} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {RestEndpoints} from "src/app/utils/RestEndpoints";
import {Router} from "@angular/router";
import {MessageService} from "src/app/services/message.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly TOKEN_HEADER = "Authorization";
  private readonly USER_TOKEN = "user";

  private userSubject: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(null);
  public user: Observable<User | null>;

  constructor(private storageService: StorageService,
              private http: HttpClient,
              private router: Router,
              private messageService: MessageService) {
    let userObject = storageService.getItemInLocal(this.USER_TOKEN);
    if (userObject) {
      this.userSubject = new BehaviorSubject<User | null>(JSON.parse(userObject));
    }

    this.user = this.userSubject.asObservable();
  }

  /**
   * Gets the value of the user if authenticated
   */
  public get userValue(): User | null {
    return this.userSubject.value;
  }

  /**
   * Registers new user into the system
   * @param user User to register
   */
  public register(user: User) {
    return this.http.post(RestEndpoints.REGISTRATION, user);
  }

  /**
   * Authenticates user and saves rest token in session storage
   * @param user User to authenticate
   */
  public authenticate(user: User): Observable<boolean | Observable<boolean>> {
    return this.http.post(RestEndpoints.LOGIN, user, {observe: 'response'}).pipe(
      map((response) => {
        let token = response.headers.get(this.TOKEN_HEADER);
        if (token != null) {
          this.storageService.saveToken(token);
          return this.http.get<User>(RestEndpoints.USER).pipe(
            map((user) => {
              this.storageService.setItemInLocal(this.USER_TOKEN, JSON.stringify(user));
              this.userSubject.next(user);
              return true;
            })
          );
        }
        return false;
      }),
      catchError(() => {
        this.messageService.addErrorMessage("Sorry, username or password does not match");
        return of(false)
      })
    );
  }

  /**
   * Logouts current user and deletes his data from session storage
   */
  public logout(): void {
    this.storageService.removeToken();
    this.storageService.removeItem(this.USER_TOKEN);

    this.userSubject.next(null);
    this.router.navigate(['']);
  }

  /**
   * Checks if user is authenticated
   *
   * @returns true if authenticated, else otherwise
   */
  public isUserAuthenticated(): boolean {
    return !!this.storageService.getToken();
  }

}
