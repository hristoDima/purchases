import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Purchase} from 'src/app/models/Purchase';
import {RestEndpoints} from "src/app/utils/RestEndpoints";
import {PaymentMethod} from "src/app/models/PaymentMethod";
import {TransactionType} from "src/app/models/TransactionType";
import {Observable} from "rxjs";
import {MessageService} from "src/app/services/message.service";
import {tap} from "rxjs/operators";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient,
              private router: Router,
              private messageService: MessageService) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    }
    this.router.onSameUrlNavigation = 'reload';
  }

  /**
   * Gets the available payment methods
   * @returns array with the PaymentMethods
   */
  getPaymentMethods(): Observable<PaymentMethod[]> {
    return this.http.get<PaymentMethod[]>(RestEndpoints.PAYMENT_METHOD);
  }

  /**
   * Gets the available transaction types
   * @returns array with TransactionTypes
   */
  getTransactionTypes(): Observable<TransactionType[]> {
    return this.http.get<TransactionType[]>(RestEndpoints.TRANSACTION_TYPES);
  }

  /**
   * Gets the available by this user purchases
   * @returns array of Purchases
   */
  getPurchases(): Observable<Purchase[]> {
    return this.http.get<Purchase[]>(RestEndpoints.PURCHASE);
  }

  /**
   * Adds new purchase in the application and redirects to the current page
   * if successful to refresh the data inside the table
   * @param purchase new purchase to add
   * @returns purchase that is added
   */
  addNewPurchase(purchase: Purchase): Observable<Purchase> {
    return this.http.post<Purchase>(RestEndpoints.PURCHASE, purchase).pipe(tap(
      () => {
        this.messageService.addSuccessMessage("Purchase added successfully!");
        this.router.navigate([this.router.url]);
      },
      () => {
        this.messageService.addErrorMessage("Error in the data you are trying to save!!");
      }
    ));
  }

  /**
   * Updates the given purchase
   * @param purchase the purchase to be updated
   * @returns the updated purchase
   */
  updatePurchase(purchase: Purchase): Observable<Purchase> {
    return this.http.put<Purchase>(RestEndpoints.PURCHASE + "/" + purchase.id, purchase).pipe(tap(
      () => {
        this.messageService.addSuccessMessage("Purchase updated successfully!")
      },
      () => {
        this.messageService.addErrorMessage("Error in the data you are trying to save!!");
      }
    ));
  }

  /**
   * Deletes the given purchase
   * @param purchase the purchase to be deleted
   */
  deletePurchase(purchase: Purchase): Observable<void> {
    return this.http.delete<void>(RestEndpoints.PURCHASE + "/" + purchase.id).pipe(tap(
      () => {
        this.messageService.addSuccessMessage("Purchase deleted successfully!")
      },
      () => {
        this.messageService.addErrorMessage("Purchase could not be deleted!!");
      }
    ));
  }

}
