import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private readonly TOKEN_KEY: string = "token";

  private sessionStorage: Storage = sessionStorage;

  private localStorage: Storage = localStorage;

  constructor() {
  }

  /**
   * Sets the key and value in the session storage
   * @param key key to save
   * @param value value for the key
   */
  public setItem(key: string, value: string) {
    this.sessionStorage.setItem(key, value);
  }

  /**
   * Gets the value of a key from session storage
   * @param key key to search for in the session storage
   * @returns value of the key if found or null
   */
  public getItem(key: string): string | null {
    return this.sessionStorage.getItem(key);
  }

  /**
   * Removes item from the session storage
   * @param key key to remove from session storage
   */
  public removeItem(key: string) {
    this.sessionStorage.removeItem(key);
    this.localStorage.removeItem(key);
  }


  /**
   * Saves authentication token in the storage
   * @param token token to be saved
   */
  public saveToken(token: string): void {
    this.localStorage.setItem(this.TOKEN_KEY, token);
  }

  /**
   * Gets the authentication token from the storage
   * @returns string with token if found, null otherwise
   */
  public getToken(): string | null {
    return this.localStorage.getItem(this.TOKEN_KEY);
  }

  /**
   * Removes the token from the storage
   */
  public removeToken() {
    this.removeItem(this.TOKEN_KEY);
  }

  /**
   * Sets key value pair in the local storage
   * @param key key to be set
   * @param value value for the given key
   */
  public setItemInLocal(key: string, value: string) {
    this.localStorage.setItem(key, value);
  }

  /**
   * Gets value of a key from the local storage
   * @param key key to search for
   * @returns string with value if found, null otherwise
   */
  public getItemInLocal(key: string): string | null {
    return this.localStorage.getItem(key);
  }

}
