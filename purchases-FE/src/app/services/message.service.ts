import {Injectable} from '@angular/core';
import {MessageService as PrimeMessageService} from 'primeng/api';

enum Severity {
  success = "success",
  info = "info",
  warn = "warn",
  error = "error"
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private messageService: PrimeMessageService) {
  }

  /**
   * Displays info message
   * @param message message that will be displayed
   */
  addInfoMessage(message: string) {
    this.messageService.add({severity: Severity.info, summary: 'Service Message', detail: message});
  }

  /**
   * Displays success message
   * @param message message that will be displayed
   */
  addSuccessMessage(message: string) {
    this.messageService.add({severity: Severity.success, summary: 'Service Message', detail: message});
  }

  /**
   * Displays warning message
   * @param message message that will be displayed
   */
  addWarningMessage(message: string) {
    this.messageService.add({severity: Severity.warn, summary: 'Service Message', detail: message});
  }

  /**
   * DIsplays error message
   * @param message message that will be displayed
   */
  addErrorMessage(message: string) {
    this.messageService.add({severity: Severity.error, summary: 'Service Message', detail: message});
  }

  /**
   * Clears displayed messages
   */
  clearMessages() {
    this.messageService.clear();
  }

}
