import {Injectable} from '@angular/core';
import {DataService} from "src/app/services/data.service";
import {Purchase} from "src/app/models/Purchase";
import {SelectItem} from "primeng/api";
import {TranslationService} from "src/app/services/translation.service";

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  private purchases: Purchase[] = new Array<Purchase>();

  constructor(private dataService: DataService,
              private translationService: TranslationService) {
    this.dataService.getPurchases().subscribe(
      result => {
        this.purchases = result;
      }
    )
  }

  /**
   * Filters all purchases by different criteria if the criteria is not null
   * @param startYear start year of years range period to filter the purchases for
   * @param endYear end year of years range period to filter the purchases for
   * @param startMonth start month of months range period to filter the purchases for
   * @param endMonth end month of months range period to filter the purchases for
   * @param startDay start day of days range period to filter the purchases for
   * @param endDay end day of days range period to filter the purchases for
   * @param paymentMethod payment method to filter the purchases for
   * @param transactionType transaction type to filter the purchases for
   * @param reason reason to filter the purchases for
   */
  getPurchasesInPeriod(startYear: Date | null, endYear: Date | null, startMonth: Date | null, endMonth: Date | null,
                       startDay: Date | null, endDay: Date | null, paymentMethod: string | null,
                       transactionType: string | null, reason: string | null): Purchase[] {
    let purchasesInPeriod: Purchase[];
    purchasesInPeriod = this.purchases
      .filter(
        purchase => {
          if (startYear && endYear) {
            let purchaseDate = new Date(purchase.date);
            return startYear.getFullYear() <= purchaseDate.getFullYear()
              && endYear.getFullYear() >= purchaseDate.getFullYear();
          }
          return true;
        }
      ).filter(
        purchase => {
          if (startMonth && endMonth) {
            let purchaseDate = new Date(purchase.date);
            let startMonth1 = new Date(startMonth.getTime());
            let endMonth1 = new Date(endMonth.getTime());
            purchaseDate.setHours(0, 0, 0, 0);
            startMonth1.setHours(0, 0, 0, 0);
            endMonth1.setHours(0, 0, 0, 0);

            purchaseDate.setDate(1);
            startMonth1.setDate(1);
            endMonth1.setDate(1);
            return startMonth1 <= purchaseDate && endMonth1 >= purchaseDate;
          }
          return true;
        }
      ).filter(
        purchase => {
          if (startDay && endDay) {
            let purchaseDate = new Date(purchase.date);
            let startDay1 = new Date(startDay.getTime());
            let endDay1 = new Date(endDay.getTime());
            purchaseDate.setHours(0, 0, 0, 0);
            startDay1.setHours(0, 0, 0, 0);
            endDay1.setHours(0, 0, 0, 0);

            return startDay1 <= purchaseDate && endDay1 >= purchaseDate;
          }
          return true;
        }
      );
    return this.filterPurchases(purchasesInPeriod, paymentMethod, transactionType, reason);
  }

  /**
   * Filters given purchases by different criteria if the criteria is not null
   * @param purchases purchases that will be filtered
   * @param paymentMethod the value of the payment method to filter the purchases for
   * @param transactionType the value of the transaction type to filter the purchases for
   * @param reason Reason to filter the purchases for
   * @returns Array containing the filtered purchases
   */
  filterPurchases(purchases: Purchase[], paymentMethod: string | null,
                  transactionType: string | null, reason: string | null): Purchase[] {
    return purchases.filter(
      purchase => {
        if (paymentMethod && paymentMethod !== "")
          return purchase.paymentMethod === paymentMethod;
        return true;
      }
    ).filter(
      purchase => {
        if (transactionType && transactionType !== "")
          return purchase.transactionType === transactionType;
        return true;
      }
    ).filter(
      purchase => {
        if (reason && reason !== "")
          return purchase.reason.toLowerCase().includes(reason.toLowerCase());
        return true;
      }
    );
  }

  /**
   * Gets all months as SelectItems; First month is indexed as 0, second as 1 and the last one as 11
   * @returns Array of SelectItem where the value is the month number(0...11) and label is
   * the translated name of the month
   */
  getMonths(): SelectItem[] {
    let monthNames = this.translationService.getTranslation("primeng.monthNames");

    let monthsAsSelectItems: SelectItem[] = [];

    for (let i = 0; i < monthNames.length; i++) {
      monthsAsSelectItems.push({value: i, label: monthNames[i]})
    }

    return monthsAsSelectItems;
  }
}
