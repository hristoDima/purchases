import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "src/app/components/home-page/home-page.component";
import {DisplayPurchasesComponent} from "src/app/components/display-purchases/display-purchases.component";
import {AuthGuard} from "src/app/guards/auth.guard";
import {StatisticsComponent} from "src/app/components/statistics/statistics.component";

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'purchases',
    component: DisplayPurchasesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'statistics/:type',
    component: StatisticsComponent,
    canActivate: [AuthGuard]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
