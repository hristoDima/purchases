import {Role} from "./Role";

export interface User {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  token: string;
  role: Role;
}
