export class Purchase {
  id: number;
  money: number;
  transactionType: string;
  paymentMethod: string;
  reason: string;
  products: string;
  date: Date;
  username: string;

  constructor(id: number, money: number, transaction_type: string, payment_method: string,
              reason: string, products: string, date: Date, username: string) {
    this.id = id;
    this.money = money;
    this.transactionType = transaction_type;
    this.paymentMethod = payment_method;
    this.reason = reason;
    this.products = products;
    this.date = date;
    this.username = username;
  }
}
