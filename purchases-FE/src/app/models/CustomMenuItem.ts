import {MenuItem} from "primeng/api";

export interface CustomMenuItem extends MenuItem {
  labelTranslationKey?: string;
  items?: CustomMenuItem[];
}
