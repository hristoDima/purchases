export class TransactionType {
  type: string;

  constructor(type: string) {
    this.type = type;
  }
}
