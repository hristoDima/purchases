export class PaymentMethod{
  paymentMethod: string;

  constructor(paymentMethod: string) {
    this.paymentMethod = paymentMethod;
  }
}
