export class Constants {
  public static readonly STATISTIC_TYPE : string = "type";
  public static readonly STATISTIC_TYPE_YEAR : string ="yearly";
  public static readonly STATISTIC_TYPE_MONTH : string ="monthly";
  public static readonly STATISTIC_TYPE_DAYS : string ="days-range";
}
