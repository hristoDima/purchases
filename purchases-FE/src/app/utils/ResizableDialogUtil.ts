import {Dialog} from "primeng/dialog";

export class ResizableDialogUtil {
  private static readonly MAXIMIZED_CLASS: string = "p-dialog-maximized";

  /**
   * Maximizes dialog by setting "p-dialog-maximized" additional class to it
   * @param dialog p-dialog element that will be maximized
   */
  public static maximizeDialog(dialog: Dialog): void {
    let dialogContainer = dialog.container;
    let containerClasses = dialogContainer?.getAttribute("class");
    if (containerClasses) {
      containerClasses += " " + this.MAXIMIZED_CLASS;
      dialogContainer?.setAttribute("class", containerClasses);
    }
  }

  /**
   * Minimizes the dialog by removing "p-dialog-maximized" class from it
   * @param dialog p-dialog element that will be maximized
   */
  public static minimizeDialog(dialog: Dialog): void {
    let dialogContainer = dialog.container;
    let containerClasses = dialogContainer?.getAttribute("class");
    if (containerClasses) {
      containerClasses = containerClasses.replace(this.MAXIMIZED_CLASS, "");
      dialogContainer.setAttribute("class", containerClasses);
    }
  }
}
