import {environment} from "src/environments/environment";

export class RestEndpoints {
  public static readonly BASE_URL = environment.API_URL;

  public static readonly PAYMENT_METHOD = RestEndpoints.BASE_URL + "/payment";
  public static readonly TRANSACTION_TYPES = RestEndpoints.BASE_URL + "/transaction";
  public static readonly PURCHASE = RestEndpoints.BASE_URL + "/purchase";

  public static readonly LOGIN = RestEndpoints.BASE_URL + "/login";
  public static readonly REGISTRATION = RestEndpoints.BASE_URL + "/registration";
  public static readonly USER = RestEndpoints.BASE_URL + "/user";
}
